# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 01:41:34 2020

@author: Md Shamsul Alam
"""

import numpy as np 
n = int(input("Enter the size of the matrix (n=):")) 
#C = int(input("Enter the number of columns:")) 
  
# Initialize matrix 
A = [] 
print("Enter the elements row wise:") 


# For user input 
for i in range(n):          # A for loop for row entries 
    P =[] 
    for j in range(n):      # A for loop for column entries 
         P.append(int(input())) 
    A.append(P) 

print("The matrix A is:")
for i in range(n): 
    for j in range(n): 
        print(A[i][j], end = " ") 
    print() 
    

A_rot = np.rot90(A, k=1, axes=(1,0))

print("The rotated matrix is", A_rot)