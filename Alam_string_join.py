# -*- coding: utf-8 -*-

"""Write a scriptthat take two strings. Remove the elements in string one that 
are also present in string two. Print the remaining elements of string 1. 
For instance: a=‘hello’b=‘tell’result=‘ho """

First_string = input("type your first word:")
Second_string = input("type your second word:")
s1 = set(First_string) #converting string into set
s2 = set(Second_string) #converting string into set
common_character = list(s1 & s2) #in order to convert set into list

#uncommon characters between two strings
uncommon_character = [i for i in First_string if i not in common_character]+[i for i in Second_string if i not in common_character]
#uncommon characters only in first string
uncommon_only_in_first_string = [i for i in First_string if i not in common_character]

list_to_str_all = ''.join(list(uncommon_character))
list_to_str_first = ''.join(list(uncommon_only_in_first_string))
print("the remaining element of string 1 is: ", list_to_str_first)

 
 