"""Author: Md Shamsul Alam"""

"""Write a program that solve a system of simultaneous equations of size n.
 where n represents the number of unknowns and number of equations. 
 The program needs to ask the user the size of the system and the 
 coefficients for each equation. Then the program will show the 
 user the result.You can calculate the result using 𝑥𝑥=𝐴𝐴−1∗𝑏𝑏     
 (-1 is an exponential) """
 
import numpy as np 
n = int(input("Enter the size of the equation:")) 
#C = int(input("Enter the number of columns:")) 
  
# Initialize matrix 
A = [] 
print("Enter the coefficients equation wise:") 


# For user input 
for i in range(n):          # A for loop for row entries 
    P =[] 
    for j in range(n):      # A for loop for column entries 
         P.append(int(input())) 
    A.append(P) 

"""One line approach:

or 
A = [[int(input()) for i in range (n)] for y in range(n)] 
 
""" 
# For printing the matrix 
print("The matrix A is:")
for i in range(n): 
    for j in range(n): 
        print(A[i][j], end = " ") 
    print() 

##For the vector b
b = []
print("Enter the constant values of the equation")

#For user input
for i in range(n):
    b.append(int(input()))
print("the b vector is:")
print (b)
x = np.dot(np.linalg.inv(A), b) 
#np.linalg.inv(A)  is simply the inverse of A, np.dot is the dot product
"""x=Ae-1*b"""
print("The solution of 𝑥 is: ")
print (x)