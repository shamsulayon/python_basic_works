# -*- coding: utf-8 -*-
"""Author: Md Shamsul Alam """

"""4.Generate 1000random numbersbetween 1 and 6Count how many one, twos, 
threes, fours, and fives where generatedPrint Results"""


import random
randomlist = []
for i in range(1,1000):
    n = random.randint(1,6)
    randomlist.append(n)
print(randomlist)
print("Number of ones generated: ", randomlist.count(1))
print("Number of twos generated: ", randomlist.count(2))
print("Number of threes generated: ", randomlist.count(3))
print("Number of fours generated: ", randomlist.count(4))
print("Number of fives generated: ", randomlist.count(5))
print("Number of sixes generated: ", randomlist.count(6))
