
"""5.Generate 1000 random numbers between 1 and 50Count how may values
 are generate in intervals of 5.For instance:How many number between 1 and 5.
 how many numbers between 6 and 10 and so on.Print the results"""
 
 
 import random
randomlist = []
for i in range(1,1000):
    n = random.randint(1,50)
    randomlist.append(n)
print(randomlist)

def count(list1, l, r): 
      
    # x for x in list1
    # the if condition checks for the number of numbers in the range  
    # l to r  it is the range
    # the return is stored in a list 
    return len(list(x for x in list1 if l <= x <= r)) 

#now how many number between 1 and 5  
l = 1
r = 5 
print ("number of numberical integers generated between 1 and 5 is",count(randomlist, l, r))
#for rest of the ranges
print ("number of numberical integers generated between 6 and 10 is",count(randomlist, 6, 10))
print ("number of numberical integers generated between 11 and 15 is",count(randomlist, 11, 15))
print ("number of numberical integers generated between 16 and 20 is",count(randomlist, 16, 20))
print ("number of numberical integers generated between 21 and 25 is",count(randomlist, 21, 25))
print ("number of numberical integers generated between 26 and 30 is",count(randomlist, 26, 30))
print ("number of numberical integers generated between 31 and 35 is",count(randomlist, 31, 35))
print ("number of numberical integers generated between 36 and 40 is",count(randomlist, 36, 40))
print ("number of numberical integers generated between 41 and 45 is",count(randomlist, 41, 45))
print ("number of numberical integers generated between 46 and 50 is",count(randomlist, 46, 50))
