# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 05:35:07 2020

@author: Md Shamsul Alam """
"""1. Generate a square matrix with random integer values of size n. 
Use plot function to plot each row of the matrix as y values.
 Plot all rows in the same matrix. Use a different color for each row. 
 Personalize your plot using labels"""
 
import numpy as np
import matplotlib.pyplot as plt
 
def sq_matrix(n):
  return np.random.rand(n,n)

square_matrix = sq_matrix(3)

#for i in range(len(square_matrix)):
#    plt.plot(square_matrix[i,])
#plt.show()


for i in range(len(square_matrix)):
    y=square_matrix[i,]
    plt.plot(y)
plt.show()